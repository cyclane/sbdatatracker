import { writable } from "svelte/store";

function createLogin() {
	const { subscribe, set, update } = writable<
		| {
				username: string;
				token: string;
		  }
		| undefined
		| null
	>(undefined);

	return {
		subscribe,
		set,
		update,
		getValue: () =>
			new Promise(resolve => {
				const unsubscribe = subscribe(value => {
					if (value !== undefined) {
						unsubscribe();
						resolve(value);
					}
				});
			})
	};
}

export const login = createLogin();

function createExtraNav() {
	const { subscribe, set, update } = writable<
		(string | [string, () => void])[] | undefined
	>();

	return {
		subscribe: (
			...params: Parameters<typeof subscribe>
		): ReturnType<typeof subscribe> => {
			const unsubscribe = subscribe(...params);
			return () => {
				set(undefined);
				unsubscribe();
			};
		},
		set,
		update
	};
}

export const extraNav = createExtraNav();

export let playerCache: Record<string, string> = {};
export let uuidCache: Record<string, string> = {};
export let profileCache: Record<string, string> = {};
