import nbt from "nbt-ts";
import gzip from "node-gzip";
import { getOnlineStatus, getProfileByUUID, Profile } from "./lib/hypixel";

export async function log(key: string, profile: string) {
	let r = await getProfileByUUID(key, profile);
	if (!r.success || !r.profile) throw new Error("No success from Hypixel");

	let d: Profile = r.profile as any;
	d.timestamp = Date.now();

	for (const member in d.members) {
		let onlineStatus = await getOnlineStatus(key, member);
		if (onlineStatus.success && onlineStatus.session) {
			d.members[member].online_status = onlineStatus.session;
			d.members[member].online_status.timestamp = Date.now();
		}

		const inventories = [
			"inv_armor",
			"quiver",
			"talisman_bag",
			"backpack_icons",
			"fishing_bag",
			"ender_chest_contents",
			"wardrobe_contents",
			"potion_bag",
			"personal_vault_contents",
			"inv_contents",
			"candy_inventory_contents"
		];
		if (d.members[member].backpack_contents) {
			for (const backpack in d.members[member].backpack_contents) {
				await decodeInventory(
					d.members[member].backpack_contents[backpack]
				);
			}
		}
		if (d.members[member].backpack_icons) {
			for (const icon in d.members[member].backpack_icons) {
				await decodeInventory(d.members[member].backpack_icons[icon]);
			}
		}
		for (const idx in inventories) {
			const inv = inventories[idx];
			await decodeInventory(d.members[member][inv]);
		}
	}

	return d;
}

async function decodeInventory(inventory: any) {
	if (inventory?.data) {
		inventory.data = nbt.decode(
			await gzip.ungzip(Buffer.from(inventory.data, "base64"))
		);
	}
}
