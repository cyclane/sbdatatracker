import { aql } from "arangojs";
import type { Document } from "arangojs/documents";
import type { IncomingMessage, ServerResponse } from "http";
import { getProfileByUUID, getProfilesByPlayer } from "../../lib/hypixel";
import { Permissions } from "../../lib/permissions";
import { authorizeRequest } from "../../lib/butil";
import { db, API_KEY } from "../../server";
import type { Tracker } from "./_types";

export async function get(req: IncomingMessage, res: ServerResponse) {
	const [_, exit] = await authorizeRequest(req, res, db, [
		Permissions.VIEW_PROFILES
	]);
	if (exit) return;

	let profiles: (Tracker & { profile: string })[] = [];
	const cursor = await db.query(aql`
		FOR doc IN config
			FILTER doc.type == "tracker"
			RETURN doc
	`);
	while (cursor.hasNext) {
		const next: Document<Tracker> = await cursor.next();
		profiles.push({
			profile: next._key,
			type: next.type,
			data: next.data
		});
	}
	res.writeHead(200);
	res.end(JSON.stringify(profiles));
}

export async function put(req: IncomingMessage, res: ServerResponse) {
	const [_, exit] = await authorizeRequest(req, res, db, [
		Permissions.ADD_PROFILE
	]);
	if (exit) return;

	const url = new URL(req.url!, "https://example.com/");
	if (!url.searchParams.get("uuid")) {
		res.writeHead(400);
		res.end(
			JSON.stringify({
				message: "uuid query parameter should be set"
			})
		);
		return;
	}

	const profile = await getProfileByUUID(
		API_KEY || "",
		url.searchParams.get("uuid")!
	);
	if (!profile.profile) {
		res.writeHead(profile._response.status);
		res.end(
			JSON.stringify({
				message: "Hypixel error"
			})
		);
		return;
	}
	const config = db.collection("config");
	let tracker: Tracker & { _key: string } = {
		_key: profile.profile!.profile_id,
		type: "tracker",
		data: {
			name: "",
			members: Object.keys(profile.profile!.members)
		}
	};
	const profiles = await getProfilesByPlayer(
		API_KEY || "",
		url.searchParams.get("member") || tracker.data.members[0]
	);
	tracker.data.name =
		profiles.profiles?.find(
			v => v.profile_id === profile.profile?.profile_id
		)?.cute_name || "";

	if (url.searchParams.get("member")) {
		tracker.data.members = [url.searchParams.get("member")!].concat(
			tracker.data.members.filter(
				m => m !== url.searchParams.get("member")
			)
		);
	}

	await config.save(tracker);
	res.writeHead(200);
	res.end(JSON.stringify(tracker));
}
