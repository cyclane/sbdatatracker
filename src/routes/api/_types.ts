export interface Config<TS = string, T = any> {
	type: TS;
	data: T;
}

export interface User {
	username: string;
	password?: string;
	permissions: string[];
}

export interface Session {
	token: string;
	exp: number;
	type: string;
	user: string;
}

// Configs
export type Tracker = Config<
	"tracker",
	{
		name: string;
		members: string[];
	}
>;

// Graphs
export type Axis = {
	name?: string;
	type?: string;
};

export type BaseGraph<TS = string, T = any> = Config<
	TS,
	T & {
		name: string;
		style: string;
		x_axis?: Axis;
		y_axis?: Axis;
	}
>;

export type Graph = BaseGraph<
	"manual_graph",
	{
		name: string;
		value: string;
	}
>;
