import type { IncomingMessage, ServerResponse } from "http";
import { authorizeRequest } from "../../lib/butil";
import { db } from "../../server";

export async function get(req: IncomingMessage, res: ServerResponse) {
	const [user, exit] = await authorizeRequest(req, res, db, []);
	if (exit) return;

	res.writeHead(200);
	res.end(JSON.stringify(user!.permissions));
}
