import type { IncomingMessage, ServerResponse } from "http";
import { parse, serialize } from "cookie";
import hat from "hat";
import { hash, compare } from "bcrypt";
import {
	accessTokenExpire,
	db,
	refreshTokenExpire,
	saltRounds
} from "../../server";
import { aql, Database } from "arangojs";
import { findSession } from "../../lib/butil";
import type { Document } from "arangojs/documents";
import type { User } from "./_types";

export async function post(req: IncomingMessage, res: ServerResponse) {
	const cookies = parse(req.headers.cookie || "");
	let user: Document<User>;

	// Check for existing refresh session
	let session = await findSession(db, cookies.refresh);
	// If session exists, refresh it
	if (session !== null && session.exp > Date.now()) {
		// Check user
		try {
			user = await db.collection("users").document(session.user);
		} catch (e) {
			res.writeHead(401);
			res.end(
				JSON.stringify({
					message: "Non-existent user"
				})
			);
			return;
		}

		const sessions = db.collection("sessions");
		await sessions.remove(session, { silent: true });
		const { token, refreshToken, tokenExp, refreshTokenExp } =
			await newSession(db, session.user);

		res.writeHead(200, {
			"Set-Cookie": serialize("refresh", refreshToken, {
				expires: new Date(refreshTokenExp),
				httpOnly: true,
				sameSite: true,
				secure: true
			})
		});
		res.end(
			JSON.stringify({
				token,
				username: user.username,
				expires: tokenExp
			})
		);
		return;
	}

	// Check for parameters
	let url = new URL(req.url!, "https://example.com/");
	if (
		url.searchParams.get("username") === null ||
		url.searchParams.get("password") === null
	) {
		res.writeHead(400);
		res.end(
			JSON.stringify({
				message:
					"Both username and password query parameters should be set"
			})
		);
		return;
	}

	// Check user
	const cursor = await db.query(aql`
		FOR user IN users
			FILTER user.username == ${url.searchParams.get("username")}
			LIMIT 1
			RETURN user
	`);
	if (!cursor.hasNext) {
		res.writeHead(401);
		res.end(
			JSON.stringify({
				message: "Non-existent user"
			})
		);
		return;
	}
	user = await cursor.next();

	// Check password
	if (!user.password) {
		res.writeHead(401);
		res.end(
			JSON.stringify({
				message: "Cannot authenticate with this user"
			})
		);
		return;
	} else if (
		!(await compare(url.searchParams.get("password")!, user.password))
	) {
		res.writeHead(401);
		res.end(
			JSON.stringify({
				message: "Invalid password"
			})
		);
		return;
	}

	// Creat new session and return
	const { token, refreshToken, tokenExp, refreshTokenExp } = await newSession(
		db,
		user._key
	);
	res.writeHead(200, {
		"Set-Cookie": serialize("refresh", refreshToken, {
			expires: new Date(refreshTokenExp),
			httpOnly: true,
			sameSite: true,
			secure: true
		})
	});
	res.end(
		JSON.stringify({
			token,
			username: user.username,
			expires: tokenExp
		})
	);
}

async function newSession(
	db: Database,
	user: string
): Promise<{
	token: string;
	tokenExp: number;
	refreshToken: string;
	refreshTokenExp: number;
}> {
	const sessions = db.collection("sessions");
	const token = hat();
	let tokenExp = Date.now() + accessTokenExpire;
	const refreshToken = hat();
	let refreshTokenExp = Date.now() + refreshTokenExpire;
	await sessions.saveAll([
		{
			token: await hash(token, saltRounds),
			exp: tokenExp,
			type: "access",
			user: user
		},
		{
			token: await hash(refreshToken, saltRounds),
			exp: refreshTokenExp,
			type: "refresh",
			user: user
		}
	]);
	return {
		token,
		tokenExp,
		refreshToken,
		refreshTokenExp
	};
}
