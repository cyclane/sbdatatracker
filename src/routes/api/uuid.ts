import type { IncomingMessage, ServerResponse } from "http";
import { getUUIDByUsername } from "../../lib/mojang";

export async function get(req: IncomingMessage, res: ServerResponse) {
	const url = new URL(req.url!, "https://example.com/");
	if (!url.searchParams.get("username")) {
		res.writeHead(400);
		res.end(
			JSON.stringify({
				message: "username query parameter should be set"
			})
		);
		return;
	}
	let response = await getUUIDByUsername(url.searchParams.get("username")!);
	res.writeHead(200);
	res.end(JSON.stringify(response));
}
