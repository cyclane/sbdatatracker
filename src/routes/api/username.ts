import type { IncomingMessage, ServerResponse } from "http";
import { getUsernameByUUID } from "../../lib/mojang";

export async function get(req: IncomingMessage, res: ServerResponse) {
	const url = new URL(req.url!, "https://example.com/");
	if (!url.searchParams.get("uuid")) {
		res.writeHead(400);
		res.end(
			JSON.stringify({
				message: "uuid query parameter should be set"
			})
		);
		return;
	}
	let response = await getUsernameByUUID(url.searchParams.get("uuid")!);
	res.writeHead(200);
	res.end(JSON.stringify(response));
}
