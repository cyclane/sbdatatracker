import type { IncomingMessage, ServerResponse } from "http";
import { authorizeRequest } from "../../../../../lib/butil";
import { Permissions } from "../../../../../lib/permissions";
import { db } from "../../../../../server";
import type { Graph } from "../../../_types";

export async function get(
	req: IncomingMessage & {
		params: { graph: string; profile: string; player: string };
	},
	res: ServerResponse
) {
	const [_, exit] = await authorizeRequest(req, res, db, [
		Permissions.VIEW_PROFILES
	]);
	if (exit) return;

	const cnf = db.collection("config");
	const profile = db.collection("c" + req.params.profile);
	if (!(await profile.exists())) {
		res.writeHead(404);
		res.end(
			JSON.stringify({
				message: "Profile not found"
			})
		);
	}

	let graph: Graph = await cnf.document(req.params.graph);
	switch (graph.type) {
		case "manual_graph":
			let data = await (
				await db.query({
					query: `FOR profile IN ${profile.name} 
						LET member = profile.members[@member_uuid]
						${graph.data.value}`,
					bindVars: {
						member_uuid: req.params.player
					}
				})
			).all();
			res.writeHead(200);
			if (graph.data.style === "line") {
				if (data[0].length === 2 && !Array.isArray(data[0][0])) {
					res.end(
						JSON.stringify(
							data.filter((value, idx) => {
								return (
									idx === 0 ||
									idx === data.length - 1 ||
									data[idx - 1][1] !== value[1] ||
									data[idx + 1][1] !== value[1]
								);
							})
						)
					);
				} else {
					let d: [string, number, number][][] = data;
					res.end(
						JSON.stringify(
							d
								.map((values, idx) => {
									return values.filter(value => {
										return (
											idx === 0 ||
											idx === d.length - 1 ||
											d[idx - 1].find(
												v => v[0] === value[0]
											)?.[2] !== value[2] ||
											d[idx + 1].find(
												v => v[0] === value[0]
											)?.[2] !== value[2]
										);
									});
								})
								.filter(v => v.length > 0)
						)
					);
				}
			} else {
				res.end(JSON.stringify(data));
			}
			return;
		default:
			res.writeHead(404);
			res.end(
				JSON.stringify({
					message: "Not a graph ID"
				})
			);
			return;
	}
}
