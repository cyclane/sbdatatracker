import type { IncomingMessage, ServerResponse } from "http";
import { authorizeRequest } from "../../../lib/butil";
import { Permissions } from "../../../lib/permissions";
import { getProfilesByPlayer } from "../../../lib/hypixel";
import { API_KEY, db } from "../../../server";

export async function get(
	req: IncomingMessage & { params: { player: string } },
	res: ServerResponse
) {
	const [_, exit] = await authorizeRequest(req, res, db, [
		Permissions.ADD_PROFILE
	]);
	if (exit) return;

	let response = await getProfilesByPlayer(API_KEY || "", req.params.player);
	if (response.success && response.profiles !== undefined) {
		res.writeHead(200);
		res.end(JSON.stringify(response.profiles));
	} else {
		res.writeHead(response._response.status);
		res.end(
			JSON.stringify({
				message: "Unsuccessful Hypixel API response"
			})
		);
	}
}
