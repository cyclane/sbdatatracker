import type { IncomingMessage, ServerResponse } from "http";
import { getProfilesByPlayer } from "../../../../../lib/hypixel";
import { API_KEY, profileNameCache } from "../../../../../server";

export async function get(
	req: IncomingMessage & { params: { player: string; profile: string } },
	res: ServerResponse
) {
	if (profileNameCache[req.params.player]?.[req.params.profile]) {
		res.writeHead(200);
		res.end(
			JSON.stringify(
				profileNameCache[req.params.player]?.[req.params.profile]
			)
		);
		return;
	}

	let response = await getProfilesByPlayer(API_KEY || "", req.params.player);
	if (response.success && response.profiles) {
		if (profileNameCache[req.params.player] === undefined) {
			profileNameCache[req.params.player] = {};
		}
		response.profiles.forEach(
			p =>
				(profileNameCache[req.params.player][p.profile_id] =
					p.cute_name)
		);
		res.writeHead(200);
		res.end(
			JSON.stringify(
				response.profiles.find(p => p.profile_id === req.params.profile)
					?.cute_name
			)
		);
	} else {
		res.writeHead(response._response.status);
		res.end(
			JSON.stringify({
				message: "Unsuccessful Hypixel API response"
			})
		);
	}
}
