import { aql } from "arangojs";
import type { Document } from "arangojs/documents";
import type { IncomingMessage, ServerResponse } from "http";
import { authorizeRequest } from "../../lib/butil";
import { checkPermissions, Permissions } from "../../lib/permissions";
import { db } from "../../server";
import type { Graph } from "./_types";

export async function get(req: IncomingMessage, res: ServerResponse) {
	const [_, exit] = await authorizeRequest(req, res, db, [
		Permissions.VIEW_PROFILES
	]);
	if (exit) return;

	let graphs: (Graph & { graph: string })[] = [];
	const cursor = await db.query(aql`
		FOR doc IN config
			FILTER CONTAINS(doc.type, "graph")
			RETURN doc
	`);
	while (cursor.hasNext) {
		const next: Document<Graph> = await cursor.next();
		graphs.push({
			graph: next._key,
			type: next.type,
			data: next.data
		});
	}
	res.writeHead(200);
	res.end(JSON.stringify(graphs));
}

export async function put(req: IncomingMessage, res: ServerResponse) {
	const [user, exit] = await authorizeRequest(req, res, db, [
		Permissions.ADD_GRAPH
	]);
	if (exit) return;

	const url = new URL(req.url!, "https://example.com/");
	if (!url.searchParams.get("name")) {
		res.writeHead(400);
		res.end(
			JSON.stringify({
				message: "name query parameter should be set"
			})
		);
		return;
	}

	if (!url.searchParams.get("style")) {
		res.writeHead(400);
		res.end(
			JSON.stringify({
				message: "style query parameter should be set"
			})
		);
		return;
	}

	if (!url.searchParams.get("type")) {
		res.writeHead(400);
		res.end(
			JSON.stringify({
				message: "type query parameter should be set"
			})
		);
		return;
	}

	switch (url.searchParams.get("type")) {
		case "manual_graph":
			if (
				!checkPermissions([Permissions.MANUAL_GRAPH], user!.permissions)
			) {
				res.writeHead(403);
				res.end(
					JSON.stringify({
						message: "You are unauthroized to make manual graphs"
					})
				);
				return;
			}
			if (!url.searchParams.get("value")) {
				res.writeHead(400);
				res.end(
					JSON.stringify({
						message: "value query parameter should be set"
					})
				);
				return;
			}
			await db.collection("config").save({
				type: "manual_graph",
				data: {
					name: url.searchParams.get("name"),
					style: url.searchParams.get("style"),
					value: url.searchParams.get("value"),
					x_axis: {
						name:
							url.searchParams.get("x_axis_name") ||
							"Date & time",
						type: url.searchParams.get("x_axis_type") || "time"
					},
					y_axis: {
						name: url.searchParams.get("y_axis_name") || undefined,
						type: url.searchParams.get("y_axis_type") || "value"
					}
				}
			} as Graph);
			res.writeHead(204);
			res.end();
			break;
		default:
			res.writeHead(400);
			res.end(
				JSON.stringify({
					message: "Invalid graph type"
				})
			);
	}
}
