import { parse, serialize } from "cookie";
import type { IncomingMessage, ServerResponse } from "http";
import { authorizeRequest, findSession } from "../../lib/butil";
import { db } from "../../server";

export async function post(req: IncomingMessage, res: ServerResponse) {
	const [user, exit] = await authorizeRequest(req, res, db, []);
	if (exit) return;

	const cookies = parse(req.headers.cookie || "");
	const refresh = await findSession(db, cookies["refresh"] || "");

	if (!user?._session || !refresh) {
		res.writeHead(400);
		res.end(
			JSON.stringify({
				message: "No session to logout"
			})
		);
		return;
	}

	const sessions = db.collection("sessions");
	await sessions.remove(refresh!);
	await sessions.remove(user!._session!);

	res.writeHead(204, {
		"Set-Cookie": serialize("refresh", "deleted", {
			expires: new Date(0),
			httpOnly: true,
			sameSite: true,
			secure: true
		})
	});
	res.end();
}
