import { authorize, requestWithDefaults } from "./util";

export type HypixelResponse<T> = {
	_response: Response;
	success: boolean;
} & T;

export const ENDPOINT = "https://api.hypixel.net/";

// Request function with rate limit handling
const request: ReturnType<typeof requestWithDefaults> = (() => {
	async function req(
		endpoint: string,
		init?: RequestInit
	): Promise<Response> {
		try {
			let response = await requestWithDefaults(ENDPOINT)(endpoint, init);
			console.log(
				`Left: ${response.headers.get(
					"ratelimit-remaining"
				)}\tReset: ${response.headers.get("ratelimit-reset")}s`
			);
			return response;
		} catch (e) {
			let response: Response = e.response;
			if (response.status === 429) {
				const reset = response.headers.get("ratelimit-reset");
				if (reset) {
					console.log("Rate limited, waiting for", reset, "seconds");
					await new Promise(resolve =>
						setTimeout(resolve, (2 + parseInt(reset, 10)) * 1000)
					);
					return req(endpoint, init);
				}
			}
			throw e;
		}
	}
	return req;
})();

/**
 * Get Hypixel API key information
 * @param key API key
 * @returns A response with the API key information
 */
export async function getAPIKeyInformation(key: string): Promise<
	HypixelResponse<{
		record?: {
			key: string;
			owner: string;
			limit: number;
			queriesInPastMin: number;
			totalQueries: number;
		};
	}>
> {
	let response = await request("/key", authorize(key));
	return {
		...(await response.json()),
		_response: response
	};
}

export type Profile = {
	profile_id: string;
	members: any;
	community_upgrades: any | null;
	cute_name: string;
	banking: any | null;
	game_mode: string | null;
	timestamp: number;
};
/**
 * Get Skyblock profiles by player
 * @param key API key
 * @param player The player's UUID
 * @returns A response with the player's profiles
 */
export async function getProfilesByPlayer(
	key: string,
	player: string
): Promise<
	HypixelResponse<{
		profiles?: {
			profile_id: string;
			members: any;
			community_upgrades: any | null;
			cute_name: string;
			banking: any | null;
			game_mode: string | null;
		}[];
	}>
> {
	let response = await request(
		`/skyblock/profiles?uuid=${player}`,
		authorize(key)
	);
	return {
		...(await response.json()),
		_response: response
	};
}

/**
 * Get Skyblock profile by its UUID
 * @param key API key
 * @param profile The profile's UUID
 * @returns A response with the profile data
 */
export async function getProfileByUUID(
	key: string,
	profile: string
): Promise<
	HypixelResponse<{
		profile?: {
			profile_id: string;
			members: any;
			community_upgrades: any | null;
			cute_name: string;
			banking: any | null;
			game_mode: string | null;
		};
	}>
> {
	let response = await request(
		`/skyblock/profile?profile=${profile}`,
		authorize(key)
	);
	return {
		...(await response.json()),
		_response: response
	};
}

/**
 * Get a player's current online status
 * @param key API key
 * @param player Player to get status of
 * @returns A response with player status data
 */
export async function getOnlineStatus(
	key: string,
	player: string
): Promise<
	HypixelResponse<{
		uuid?: string;
		session?: {
			online: boolean;
			gameType: string;
			mode: string;
			map: string;
		};
	}>
> {
	let response = await request(`/status?uuid=${player}`, authorize(key));
	return {
		...(await response.json()),
		_response: response
	};
}
