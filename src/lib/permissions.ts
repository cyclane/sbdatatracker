export enum Permissions {
	ALL = "*",

	VIEW_PROFILES = "view_profiles",
	ADD_PROFILE = "add_profile",
	REMOVE_PROFILE = "remove_profile",

	ADD_GRAPH = "add_graph",
	MANUAL_GRAPH = "manual_graph", // separate because allows for remote code execution
	REMOVE_GRAPH = "remove_graph",

	VIEW_USERS = "view_users",
	ADD_USER = "add_user",
	EDIT_USER = "edit_user",
	REMOVE_USER = "remove_user"
}

/**
 * Check permissions
 * @param required The permission required
 * @param has The permissions to check against
 * @returns Whether the required permissions are fulfilled
 */
export function checkPermissions(required: string[], has: string[]): boolean {
	if (has.includes("*")) return true;

	return !required.some(req => {
		if (!has.includes(req)) return true;
	});
}
