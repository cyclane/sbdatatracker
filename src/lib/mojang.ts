import { requestWithDefaults } from "./util";

export const ENDPOINT = "https://api.mojang.com/";
const request = requestWithDefaults(ENDPOINT);

/**
 * Get a Minecraft username by UUID
 * @param uuid Player's UUID
 * @returns A username
 */
export async function getUsernameByUUID(uuid: string): Promise<string> {
	let response = await request(`/user/profile/${uuid}`);
	return (await response.json()).name;
}

/**
 * Get a Minecraft UUID by username
 * @param name Player's username
 * @returns A UUID
 */
export async function getUUIDByUsername(name: string): Promise<string> {
	let response = await request(`/users/profiles/minecraft/${name}`);
	return (await response.json()).id;
}
