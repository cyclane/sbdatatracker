import type { Graph, Tracker } from "../routes/api/_types";
import { playerCache, profileCache, uuidCache } from "../components/stores";
import type { Profile } from "./hypixel";
import { requestWithDefaults } from "./util";

const request = requestWithDefaults();

/**
 * Login into SBDataTracker backend
 * @param params Login parameters (no parameters = refresh)
 * @returns Login details
 */
export async function postLogin(params?: {
	username: string;
	password: string;
}): Promise<{
	token: string;
	username: string;
	expires: number;
}> {
	let url = "/api/login";
	if (params) {
		url += `?username=${encodeURIComponent(
			params.username
		)}&password=${encodeURIComponent(params.password)}`;
	}
	let response = await request(url, { method: "POST" });
	return response.json();
}

/**
 * Logout
 * @param token Authorization token
 */
export async function postLogout(token: string) {
	await request("/api/logout", {
		method: "POST",
		headers: { Authorization: token }
	});
}

/**
 * Get a list of profiles
 * @param token Authorization token
 * @returns A list of profiles
 */
export async function getProfiles(
	token?: string
): Promise<(Tracker & { profile: string })[]> {
	let response = await request(
		"/api/profiles",
		token ? { headers: { Authorization: token } } : {}
	);
	return response.json();
}

/**
 * Get a list of skyblock profiles by player
 * @param token Authorization token
 * @param player Player's UUID
 * @returns A list of profiles
 */
export async function getPlayerProfiles(
	token: string | undefined,
	player: string
): Promise<Profile[]> {
	let response = await request(
		`/api/profiles/${player}`,
		token
			? {
					headers: { Authorization: token }
			  }
			: {}
	);
	return response.json();
}

/**
 * Get a palayer's profile cute name
 * @param player Player's UUID
 * @param profile Profile's UUID
 * @returns Profile's cute name
 */
export async function getProfileCuteName(
	player: string,
	profile: string
): Promise<string> {
	if (profileCache[player + profile] !== undefined) {
		return profileCache[player + profile];
	}
	let response = await request(
		`/api/profiles/${player}/${profile}/cute-name`
	);
	let cuteName = await response.json();
	profileCache[player + profile] = cuteName;
	return cuteName;
}

/**
 * Get a list of graphs
 * @param token Authorization token
 * @returns A list of profiles
 */
export async function getGraphs(
	token?: string
): Promise<(Graph & { graph: string })[]> {
	let response = await request(
		"/api/graphs",
		token ? { headers: { Authorization: token } } : {}
	);
	return response.json();
}

/**
 * Get a player username by UUID
 * @param uuid Player's UUID
 * @returns Player's username
 */
export async function getUsername(uuid: string): Promise<string> {
	if (playerCache[uuid] !== undefined) {
		return playerCache[uuid];
	}
	let response = await request(`/api/username?uuid=${uuid}`);
	let username = await response.json();
	playerCache[uuid] = username;
	return username;
}

/**
 * Get a player UUID by username
 * @param username Player's username
 * @returns Player's UUID
 */
export async function getUUID(username: string): Promise<string> {
	if (uuidCache[username] !== undefined) {
		return uuidCache[username];
	}
	let response = await request(`/api/uuid?username=${username}`);
	let uuid = await response.json();
	uuidCache[username] = uuid;
	return uuid;
}

/**
 * Create a profile
 * @param token Authorization token
 * @param uuid Profile UUID
 * @param member Member to assign the profile to
 * @returns Tracker information
 */
export async function putProfiles(
	token: string | undefined,
	uuid: string,
	member?: string
): Promise<Tracker & { _key: string }> {
	let response = await request(
		`/api/profiles?uuid=${uuid}` + (member ? `&member=${member}` : ""),
		{
			method: "PUT",
			...(token ? { headers: { Authorization: token } } : {})
		}
	);
	return response.json();
}

/**
 * Get permissions of authenticated user
 * @param token Authorization token
 * @returns Array of permissions
 */
export async function getPermissions(token?: string): Promise<string[]> {
	let response = await request(
		"/api/permissions",
		token ? { headers: { Authorization: token } } : {}
	);
	return response.json();
}

/**
 * Create a graph
 * @param token Authorization token
 * @param type Graph type
 * @param name Graph name
 * @param style Graph style
 * @param value AQL code for graph
 * @param xAxisName X-axis name
 * @param xAxisType X-axis type
 * @param yAxisName Y-axis name
 * @param yAxisType Y-axis type
 */
export async function putGraph(
	token: string | undefined,
	type: string,
	name: string,
	style: string,
	value: string,
	xAxisName: string = "Date & time",
	xAxisType: string = "time",
	yAxisName: string = "",
	yAxisType: string = "value"
): Promise<void> {
	let searchParams = new URLSearchParams({
		type,
		name,
		style,
		value,
		x_axis_name: xAxisName,
		x_axis_type: xAxisType,
		y_axis_name: yAxisName,
		y_axis_type: yAxisType
	});
	await request("/api/graphs?" + searchParams.toString(), {
		method: "PUT",
		...(token ? { headers: { Authorization: token } } : {})
	});
	return;
}

/**
 * Get graph data for a profile
 * @param token Authorization token
 * @param graph Graph's ID
 * @param profile Profile's UUID
 * @param member Player's UUID
 * @returns Graph data
 */
export async function getGraph(
	token: string | undefined,
	graph: string,
	profile: string,
	member: string
): Promise<([number, number] | [string, number, number][])[]> {
	let response = await request(
		`/api/graphs/${graph}/${profile}/${member}`,
		token ? { headers: { Authorization: token } } : {}
	);
	return response.json();
}
