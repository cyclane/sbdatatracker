# Skyblock Data Tracker

A Hypixel Skyblock data tracker to track any data of any profile.

## ! WARNING !

This project is incomplete and is very buggy in places.

Although it should now be "safe" to use publicly, it has not been double-checked for security.

## Why?

I made this to be able to easily track other players' activity, which is useful when in competition with a specific player.

## Disclaimer

This tool can be used to track players even with all API settings disabled and should never be used for any harmful purposes.

## Prerequisites

-   [Node.js](https://nodejs.org/)
-   [ArangoDB](https://www.arangodb.com/)
-   [Hypixel API key](https://api.hypixel.net/#section/Authentication)
